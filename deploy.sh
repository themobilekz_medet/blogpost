cd ~/blogpostapi

git pull origin master
npm install
pm2 start "npm start" --name "blog_web"
pm2 start "npm run api" --name "blog_api"