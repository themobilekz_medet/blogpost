const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.send([1, 2, 3])
})

app.listen(4022, () => console.log('open http://localhost:4022'))
